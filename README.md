# ApplicantCv

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.3. This project already build and stored in the `backend/angular`.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running Build Project

Run `npm install` in the root directory. Then Navigate to `backend/` and run `npm run start` to start the build project.

## About This Project

This project porpuse is to save Applicant CV to the recruiter system. The applicant can upload their photo, personal data, educations, and working experience.

## Next

1. Upload Applicant CV to DB
2. Applicant can edit and export their uploaded CV
3. Recruiter can show Applicant CV
4. Recruiter can export uploaded CV by Applicant
