import { FormCvComponent } from './layouts/form-cv/form-cv.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: FormCvComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./modules/form-cv/form-cv.module').then(m => m.FormCvModule)
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
