import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MatSnackBar } from '@angular/material/snack-bar';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-cv',
  templateUrl: './form-cv.component.html',
  styleUrls: ['./form-cv.component.scss']
})
export class FormCvComponent implements OnInit {

  form!: FormGroup;
  titlePage = 'Curriculum Vitae'

  minName = 10;
  maxName = 100;
  minNumber = 10;
  maxNumber = 13;
  textBirthDate = '';
  minBirthDate = new Date();
  imagePreview?: string | ArrayBuffer;
  imgText = 'Url Upload Image Cover';

  constructor(private snackbar: MatSnackBar,) { }

  get educations() {
    return this.form.get('educations') as FormArray;
  }

  get workings() {
    return this.form.get('workings') as FormArray;
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      fullname: new FormControl('', {
        validators: [
          Validators.required,
          Validators.minLength(this.minName),
          Validators.maxLength(this.maxName),
        ],
      }),
      email: new FormControl('', {
        validators: [
          Validators.required,
          Validators.email,
        ],
      }),
      phone: new FormControl('', {
        validators: [
          Validators.required,
          Validators.minLength(this.minNumber),
          Validators.maxLength(this.maxNumber),
        ],
      }),
      gender: new FormControl('', {}),
      image: new FormControl(null, {
        validators: [Validators.required]
      }),
      birthdate: new FormControl({ value: '', readonly: true }, {
        validators: [Validators.required, Validators.minLength(8), Validators.maxLength(10)]
      }),
      educations: new FormArray([]),
      workings: new FormArray([]),
    });
  }

  onRemoveEducationForm(index: number) {
    this.educations.removeAt(index);
  }

  onAddEducationForm() {
    this.onAddEducationField(1);
  }

  onRemoveWorkingForm(index: number) {
    this.workings.removeAt(index);
  }

  onAddWorkingForm() {
    this.onAddWorkingField(1);
  }

  onSelectFile(event: Event) {
    const files = (event.target as HTMLInputElement).files;
    if (files && files[0]) {
      const file = files[0];
      if (file.size < 1200000) {
        this.form?.patchValue({image: file});
        this.form?.get('image')?.updateValueAndValidity();
        const reader = new FileReader();
        reader.onload = () => {
          this.imagePreview = reader.result!;
        };
        reader.readAsDataURL(file);
        this.imgText = file.name;
      } else {
        this.snackbar.open('Maximum File Size: 1MB', '', {
          duration: 3000,
          panelClass: 'error-notif',
          verticalPosition: 'top',
        });
      }
    }
  }

  onChangeBirthDate(event: MatDatepickerInputEvent<Date>) {
    this.textBirthDate = this.formatDate(event.value, false);
    this.form?.patchValue({birthdate: this.textBirthDate});
    this.form?.get('birthdate')!.updateValueAndValidity();
  }

  onValidatePostData() {
    if (
      this.form?.value.fullname.length < this.minName ||
      this.form?.value.fullname.length > this.maxName ||
      this.form?.value.phone.length < this.minNumber ||
      this.form?.value.phone.length > this.maxNumber ||
      !this.form?.value.gender || !this.form?.value.image ||
      this.form?.value.birthdate.length === 0
    ) {
      this.snackbar.open(
        'Invalid Value! Please check Minimum and Maximum Characters!',
        '',
        {
          duration: 3000,
          panelClass: 'error-notif',
          verticalPosition: 'top',
        }
      );
      return;
    }

    let isEducationValid = true;
    if (this.educations.length > 0) {
      for (let i = 0; i < this.educations.length; i++) {
        if (this.educations.controls[i].get('schoolName')?.value.length < 5 ||
          this.educations.controls[i].get('level')?.value.length < 5 ||
          this.educations.controls[i].get('startYearEdu')?.value.length < 4
        ) {
          isEducationValid = false;
        }
        if (
          !this.educations.controls[i].get('stillLearning')?.value &&
          this.educations.controls[i].get('endYearEdu')?.value === ''
        ) {
          this.snackbar.open('Please fill all Field!', '', {
            duration: 3000,
            panelClass: 'error-notif',
            verticalPosition: 'top'
          });
          isEducationValid = false;
        }
        if (this.educations.controls[i].get('stillLearning')?.value) {
          this.educations.controls[i].patchValue({endYearEdu: 'Present'})
        }
      }
    }
    if (!isEducationValid) {
      this.snackbar.open('Education not Valid!', '', {
        duration: 3000,
        panelClass: 'error-notif',
        verticalPosition: 'top'
      });
      return;
    }

    let isWorkingValid = true;
    if (this.workings.length > 0) {
      for (let i = 0; i < this.workings.length; i++) {
        if (this.workings.controls[i].get('workPlace')?.value.length < 5 ||
          this.workings.controls[i].get('workDepartment')?.value.length < 5 ||
          this.workings.controls[i].get('workPosition')?.value.length < 5 ||
          this.workings.controls[i].get('startYearWork')?.value.length < 4
        ) {
          isWorkingValid = false;
        }
        if (
          !this.workings.controls[i].get('stillWorking')?.value &&
          this.workings.controls[i].get('endYearWork')?.value === ''
        ) {
          this.snackbar.open('Please fill all Field!', '', {
            duration: 3000,
            panelClass: 'error-notif',
            verticalPosition: 'top'
          });
          isWorkingValid = false;
        }
        if (this.workings.controls[i].get('stillWorking')?.value) {
          this.workings.controls[i].patchValue({endYearWork: 'Present'})
        }
      }
    }
    if (!isWorkingValid) {
      this.snackbar.open('Working Experience not Valid!', '', {
        duration: 3000,
        panelClass: 'error-notif',
        verticalPosition: 'top'
      });
      return;
    }

    // this.onCreatePost();
    console.log(this.form.value);
  }

  private formatDate(date: any, isSetTimezone: boolean = true) {
    const d = new Date(date);
    if (isSetTimezone) {
      d.setHours(d.getHours() - 8);
    }
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    const year = d.getFullYear();

    if (month.length < 2) { month = '0' + month; }
    if (day.length < 2) { day = '0' + day; }

    return [year, month, day].join('-');
  }

  private onAddEducationField(num: number) {
    for (let i = 0; i < num; i++) {
      const group = new FormGroup({
        schoolName: new FormControl('', {
          validators: [Validators.required, Validators.minLength(5)],
        }),
        level: new FormControl('', {
          validators: [Validators.required],
        }),
        startYearEdu: new FormControl('', {
          validators: [Validators.required, Validators.minLength(4), Validators.maxLength(4)]
        }),
        endYearEdu: new FormControl('', {}),
        stillLearning: new FormControl(false, {}),
      });

      this.educations.push(group);
    }
  }

  private onAddWorkingField(num: number) {
    for (let i = 0; i < num; i++) {
      const group = new FormGroup({
        workPlace: new FormControl('', {
          validators: [Validators.required, Validators.minLength(5)],
        }),
        workDepartment: new FormControl('', {
          validators: [Validators.required, Validators.minLength(5)],
        }),
        workPosition: new FormControl('', {
          validators: [Validators.required, Validators.minLength(5)],
        }),
        startYearWork: new FormControl('', {
          validators: [Validators.required, Validators.minLength(4), Validators.maxLength(4)]
        }),
        endYearWork: new FormControl('', {}),
        stillWorking: new FormControl(false, {}),
      });

      this.workings.push(group);
    }
  }

}
