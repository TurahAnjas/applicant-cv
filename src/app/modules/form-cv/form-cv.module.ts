import { SharedModule } from './../../shared/shared.module';
import { FormCvComponent } from './form-cv.component';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from 'src/app/angular-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormCvRoutingModule } from './form-cv-routing.module';


@NgModule({
  declarations: [
    FormCvComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormCvRoutingModule,
    AngularMaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class FormCvModule { }
