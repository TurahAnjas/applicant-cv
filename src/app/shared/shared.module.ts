import { HeaderTitleComponent } from './header-title/header-title.component';
import { PageContentComponent } from './page-content/page-content.component';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from '../angular-material.module';

@NgModule({
  declarations: [
    PageContentComponent,
    HeaderTitleComponent
  ],
  imports: [
    CommonModule,
    AngularMaterialModule,
    RouterModule
  ],
  exports: [
    PageContentComponent,
    HeaderTitleComponent
  ]
})
export class SharedModule { }
